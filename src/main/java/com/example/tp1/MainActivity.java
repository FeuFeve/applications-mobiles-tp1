package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // STEP 1
        final ListView listView = (ListView) findViewById(R.id.animalList);
        String[] values = AnimalList.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        listView.setAdapter(adapter);


        // STEP 2
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                String animalName = (String) parent.getItemAtPosition(position);

                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);

                intent.putExtra("animalName", animalName);
                startActivity(intent);
            }
        });
    }
}
