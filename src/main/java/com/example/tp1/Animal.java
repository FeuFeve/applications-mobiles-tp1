package com.example.tp1;

public class Animal {

    private int highestLifespan; // years
    private String imgFile;
    private int gestationPeriod; // days
    private float birthWeight; // kg
    private int adultWeight; // kg
    private String conservationStatus;

    public Animal(int highestLifespan, String imgFile, int gestationPeriod, float birthWeight, int adultWeight, String conservationStatus) {
        this.highestLifespan = highestLifespan;
        this.imgFile = imgFile;
        this.gestationPeriod = gestationPeriod;
        this.birthWeight = birthWeight;
        this.adultWeight = adultWeight;
        this.conservationStatus = conservationStatus;
    }

    public int getHighestLifespan() {
        return highestLifespan;
    }

    public String getStrHightestLifespan() {
        return Integer.toString(highestLifespan)+" années";
    }

    public void setHighestLifespan(int highestLifespan) {
        this.highestLifespan = highestLifespan;
    }

    public String getImgFile() {
        return imgFile;
    }

    public void setImgFile(String imgFile) {
        this.imgFile = imgFile;
    }

    public int getGestationPeriod() {
        return gestationPeriod;
    }

    public String getStrGestationPeriod() {
        return Integer.toString(gestationPeriod)+" jours";
    }

    public void setGestationPeriod(int gestationPeriod) {
        this.gestationPeriod = gestationPeriod;
    }

    public float getBirthWeight() {
        return birthWeight;
    }

    public String getStrBirthWeight() {
        return Float.toString(birthWeight)+" kg";
    }

    public void setBirthWeight(int birthWeight) {
        this.birthWeight = birthWeight;
    }

    public int getAdultWeight() {
        return adultWeight;
    }

    public String getStrAdultWeight() {
        return Integer.toString(adultWeight)+" kg";
    }

    public void setAdultWeight(int adultWeight) {
        this.adultWeight = adultWeight;
    }

    public String getConservationStatus() {
        return conservationStatus;
    }

    public void setConservationStatus(String conservationStatus) {
        this.conservationStatus = conservationStatus;
    }

}
