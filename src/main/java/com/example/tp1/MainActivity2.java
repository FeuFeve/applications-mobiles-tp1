package com.example.tp1;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity2 extends AppCompatActivity {

    final String[] items = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // STEP 3
        final RecyclerView recyclerView = findViewById(R.id.animalList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(new IconicAdapter());
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            holder.bindModel(items[position]);
        }

        @Override
        public int getItemCount() {
            return (items.length);
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView label;
        ImageView icon;

        RowHolder(View row) {
            super(row);

            row.setOnClickListener(this);

            label = row.findViewById(R.id.label);
            icon = row.findViewById(R.id.icon);
        }

        @Override
        public void onClick(View v) {
            final String animalName = label.getText().toString();

            Intent intent = new Intent(MainActivity2.this, AnimalActivity.class);

            intent.putExtra("animalName", animalName);
            startActivity(intent);
        }

        void bindModel(String item) {
            label.setText(item);

            final Animal animal = AnimalList.getAnimal(item);
            Resources res = getResources();
            String drawableName = animal.getImgFile();
            int resID = res.getIdentifier(drawableName , "drawable", getPackageName());
            Drawable drawable = res.getDrawable(resID);
            icon.setImageDrawable(drawable);
        }
    }
}
