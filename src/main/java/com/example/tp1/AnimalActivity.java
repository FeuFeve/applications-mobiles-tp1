package com.example.tp1;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AnimalActivity extends AppCompatActivity {

    // STEP 2
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent = getIntent();
        String animalName = intent.getStringExtra("animalName");

        final Animal animal = AnimalList.getAnimal(animalName);

        // Animal name
        final TextView name = findViewById(R.id.name);
        name.setText(animalName);

        // Icon
        final ImageView icon = findViewById(R.id.icon);

        Resources res = getResources();
        String drawableName = animal.getImgFile();
        int resID = res.getIdentifier(drawableName , "drawable", getPackageName());
        Drawable drawable = res.getDrawable(resID);
        icon.setImageDrawable(drawable);

        // Highest lifespan
        final TextView highestLifespan = findViewById(R.id.highestLifespan);
        highestLifespan.setText(animal.getStrHightestLifespan());

        // Gestation period
        final TextView gestationPeriod = findViewById(R.id.gestationPeriod);
        gestationPeriod.setText(animal.getStrGestationPeriod());

        // Birth weight
        final TextView birthWeight = findViewById(R.id.birthWeight);
        birthWeight.setText(animal.getStrBirthWeight());

        // Adult weight
        final TextView adultWeight = findViewById(R.id.adultWeight);
        adultWeight.setText(animal.getStrAdultWeight());

        // Conservation status
        final TextView conservationStatus = findViewById(R.id.conservationStatus);
        conservationStatus.setText(animal.getConservationStatus());

        // Save button
        final Button saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newConservationStatus = conservationStatus.getText().toString();
                animal.setConservationStatus(newConservationStatus);
            }
        });

    }
}
